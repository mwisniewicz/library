from .person import User

class Biblioteka:
    """ Klasa reprezentująca bibliotekę,
    która grupuje książki i użytkowników """
    
    def __init__(self, name, address):
        self.name = name
        self.address = address
        self.books = []
        self.users = []

    def show_books(self):
        if not self.books:
            print('Pusta baza książek!')
        else:
            print(self.books)

    def add_book(self):
        title = input("Tytuł: ")
        author = input("Autor: ")
        id = self.set_book_id()
        book = Book(id=id, title=title, author=author)
        self.books.append(book)
        print(f"Książka {book} dodana do bazy!")

    def delete_book(self):
        id = int(input("Podaj ID książki do usunięcia: "))
        for index, book in enumerate(self.books):
            if id == book.id:
                del self.books[index]
                del book

    def get_book(self):
        book_ = None
        user_ = None

        book_id = int(input("Podaj ID książki: "))
        for index, book in enumerate(self.books):
            if book_id == book.id:
                book_ = book
        if not book_:
            print("Ksiązki nie ma w bazie!")

        user_id = int(input("Podaj ID użytkownika"))
        for index, user in enumerate(self.users):
            if user_id == user.id:
                user_ = user
        if not user_:
            print("Brak użytkownika w bazie!")
        else:
            user_.books.append(book_)
            book_.status = 'unavailable'
        print(f"{user_} wypożycza książkę {book_}")

    def add_user(self):
        first_name = input("Imię: ")
        last_name = input("Nazwisko: ")
        id = self.set_user_id()
        user = User(id=id, first_name=first_name, last_name=last_name)
        self.users.append(user)
        print(f"Użytkownik {user.first_name} {user.last_name} dodany do bazy!")

    def set_book_id(self):
        if not self.books:
            return 1
        max_id = 0
        for book in self.books:
            if book.id > max_id:
                max_id = book.id
        return max_id + 1

    def set_user_id(self):
        if not self.users:
            return 1
        max_id = 0
        for user in self.users:
            if user.id > max_id:
                max_id = user.id
        return max_id + 1

    def show_users(self):
        if not self.users:
            print('Pusta baza użytkowników!')
        else:
            print(self.users)


class Book:
    
    def __init__(self, id, title, author):
        self.id = id
        self.title = title
        self.author = author
        self.status = 'available'
        self.user = None
        self.return_date = None

    def __str__(self):
        return f"({self.id}) {self.title} - {self.author} ({self.status})"

    def __repr__(self):
        return f"({self.id}) {self.title} - {self.author} ({self.status})"